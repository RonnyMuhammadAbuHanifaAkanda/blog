@extends('layouts.backend.app')

@section('title', 'Posts')

@push('css')
   <link href="{{asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@endpush

@section('content')
<div class="container-fluid">
    <div class="block-header">
        <a href="{{ route('admin.post.create') }}" class="btn btn-primary waves-effect"><i class="material-icons">add</i>
            <span>ADD NEW POST</span>
        </a>
    </div>
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        ALL POST
                        <span class="badge bg-blue">{{ $posts->count() }}</span>
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Author</th>
                                    <th><i class="material-icons">visibility</i></th>
                                    <th>Is Approved</th>
                                    <th>Status</th>
                                    <th>Created_at</th>
                                    <th>Updated_at</th>
                                    <th>Actiont</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Author</th>
                                    <th><i class="material-icons">visibility</i></th>
                                    <th>Is Approved</th>
                                    <th>Status</th>
                                    <th>Created_at</th>
                                    <th>Updated_at</th>
                                    <th>Actiont</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach($posts as $key=>$post)
                                <tr>
                                    <td>{{ $post->id }}</td>
                                    <td>{{ str_limit($post->title,'10') }}</td>
                                    <td>{{ $post->user->name }}</td>
                                    <td>{{ $post->view_count }}</td>
                                    <td>
                                        @if($post-> is_approved == true)
                                            <span class="badge bg-blue">Approved</span>
                                        @else
                                            <span class="badge bg-green">Pending</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($post-> status == true)
                                            <span class="badge bg-blue">Confirm</span>
                                        @else
                                            <span class="badge bg-green">Pending</span>
                                        @endif
                                    </td>
                                    <td>{{ $post->created_at->toFormattedDateString()  }}</td>
                                    <td>{{ $post->updated_at->toFormattedDateString()  }}</td>
                                    <td class="text-center">
                                         @if($post->is_approved == true)
                                            <button type="button" class="btn btn-warning btn-sm waves-effect" disabled="disabled">
                                                <span class="label bg-green">Approved</span>
                                            </button>
                                        @else
                                            <button type="button" class="btn btn-warning btn-sm waves-effect" onclick="approvePost({{ $post->id }})">
                                                <i class="material-icons">done</i>
                                            </button>
                                                <form action="{{route('admin.post.approve', $post->id)}}" method="post" id="approve-form" style="display: none;">
                                                    @csrf
                                                    @method('PUT')
                                                </form>
                                        @endif
                                         <a href="{{route('admin.post.show',$post->id)}}" class="btn btn-primary btn-sm waves-effect"><i class="material-icons">visibility</i>
                                        </a>
                                        <a href="{{route('admin.post.edit',$post->id)}}" class="btn btn-primary btn-sm waves-effect"><i class="material-icons">edit</i>
                                        </a>
                                        <button class="btn btn-danger btn-sm waves-effect" type="button" onclick="deletePost( {{$post->id}} )" ><i class="material-icons">delete</i>
                                        </button>
                                        <form id="delete-form-{{ $post->id }}" action="{{ route('admin.post.destroy', $post->id) }}" method="POST" style="display: none;">
                                            @csrf
                                            @method('DELETE')
                                            
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
</div>
@endsection

@push('js')
    <script src="{{asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{asset('assets/backend/js/admin.js')}}"></script>
    <script src="{{asset('assets/backend/js/pages/tables/jquery-datatable.js')}}"></script>
    <script src="{{asset('assets/backend/js/sweetalert.js')}}"></script>
    <script type="text/javascript">
        function deletePost(id){
            swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!',
              cancelButtonText: 'No, cancel!',
              confirmButtonClass: 'btn btn-success',
              cancelButtonClass: 'btn btn-danger',
              buttonsStyling: false,
              reverseButtons: true
            }).then((result) => {
              if (result.value) {
                event.preventDefault();
                document.getElementById('delete-form-'+id).submit();
              } else if (
                // Read more about handling dismissals
                result.dismiss === swal.DismissReason.cancel
              ) {
                swal(
                  'Cancelled',
                  'Your file is safe :)',
                  'error'
                )
              }
            })
        }

        function approvePost(id){
            swal({
              title: 'Are you sure?',
              text: "You want to approve this post!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, approve it!',
              cancelButtonText: 'No, cancel!',
              confirmButtonClass: 'btn btn-success',
              cancelButtonClass: 'btn btn-danger',
              buttonsStyling: false,
              reverseButtons: true
            }).then((result) => {
              if (result.value) {
                event.preventDefault();
                document.getElementById('approve-form').submit();
              } else if (
                // Read more about handling dismissals
                result.dismiss === swal.DismissReason.cancel
              ) {
                swal(
                  'Cancelled',
                  'Your post is not approve :)',
                  'error'
                )
              }
            })
        }

</script>
@endpush
